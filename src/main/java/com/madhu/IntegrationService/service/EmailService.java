package com.madhu.IntegrationService.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.madhu.IntegrationService.dto.EmailDto;

@Service
public class EmailService {
	
	@Autowired 
	private JavaMailSender javaMailSender;
	
	public String sendEmail(EmailDto emailDto) {

		try {
			SimpleMailMessage mailMessage = new SimpleMailMessage();

			// Setting up necessary details
			mailMessage.setFrom(emailDto.getFrom());
			mailMessage.setTo(emailDto.getTo());
			mailMessage.setText(emailDto.getBody());
			mailMessage.setSubject(emailDto.getSubject());

			// Sending the mail
			javaMailSender.send(mailMessage);
			return "success";
		} catch (Exception e) {
			return "Failure";
		}
	}
}
