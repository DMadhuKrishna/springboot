package com.madhu.IntegrationService.dto;

public class EmailDto 
{
	private String subject;
	private String body;
	private String to;
	private String from;
	
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	@Override
	public String toString() {
		return "EmailDto [subject=" + subject + ", body=" + body + ", to=" + to + ", from=" + from + "]";
	}
	
	
}
