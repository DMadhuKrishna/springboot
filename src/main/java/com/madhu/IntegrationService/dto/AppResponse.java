package com.madhu.IntegrationService.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class AppResponse<T> 
{
	private String statusCode;
	private String status;
	private T data;
	private String error;
	
}
