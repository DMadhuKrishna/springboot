package com.madhu.IntegrationService.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.madhu.IntegrationService.AppConstants;
import com.madhu.IntegrationService.dto.EmailDto;
import com.madhu.IntegrationService.service.EmailService;

@RestController
@RequestMapping(value=AppConstants.FORWARD_SLASH)
public class EmailController 
{
	@Autowired
	private EmailService emailService;
	
	
	@PostMapping(value=AppConstants.SENDING_EMAIL)
	public String processEmail(@RequestBody EmailDto emailDto) 
	{
		System.out.println(emailDto);
		return  emailService.sendEmail(emailDto);
	}
	
}
